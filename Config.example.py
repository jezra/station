Config = {
  "volume" : "10",
  "streams":
  [
    {
      "name": "KMFA", 
      "location": "Austin, Texas", 
      "audioSource":"https://kmfa.streamguys1.com/KMFA-mp3", 
    },
    {
      "name": "KVMR", 
      "location": "Nevada City, California", 
      "audioSource": "http://live.kvmr.org:8000/dial", 
    },
    {
      "name": "KCMS",
      "location": "San Mateo, California",
      "audioSource": "http://ice7.securenetsystems.net/KCSM2", 
    },
    {
      "name": "KPCA",
      "location": "Petaluma, California",
      "audioSource":"http://tektite.streamguys1.com:5200/live-mp3",
    },
    {
      "name": "KXJZ",
      "location": "Sacramento, California",
      "audioSource":"http://playerservices.streamtheworld.com/api/livestream-redirect/KXJZ.mp3",
    }
  ]
}
