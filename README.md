# Station

An audio stream player with a network accessible API, written in Python, utilizing [Bottle](http://bottlepy.org/) for serving the API and [gstreamer](https://gstreamer.freedesktop.org/)  for audio playback

## Requirements 

* Python interpreter  
* Gstreamer   
* PyGObject
* [Bottle](http://bottlepy.org/) web framework   
* python gevent 

## Setup
* mv Config.example.py to Config.py and edit accordingly

	* volume: the initial volume of the audio player on a scale of 0-100
	* streams: an array of objects describing web streams
	
* install [Bottle](http://bottlepy.org/)  framework. I tend to just copy the single *bottle.py* file to the same directory as Station.py. 
  `curl https://raw.githubusercontent.com/bottlepy/bottle/master/bottle.py > bottle.py`
