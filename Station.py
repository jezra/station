#!/usr/bin/env python

# copyright 2019 Jezra
# Station
# Licensed GPL v3

''' import '''
import sys, os

#use gevent, and monkey patch!
from gevent import monkey
monkey.patch_all()

# import bottle stuff https://bottlepy.org/
from bottle import Bottle, run, static_file, request

# import GstreamerPlayer
from Player import Player

#where is this script?
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
#change to the Script directory
os.chdir(SCRIPT_DIR)

from Config import Config

# defaults
Config['current_stream_index'] = None

# create a player and set volume to the Config volume setting
player = Player(volume=Config['volume'])

# create a bottle app
app = Bottle()

''' the routes for the app '''
@app.route('/')
def root():
  return serve_static("index.html")

@app.route('/<filepath:path>')
def serve_static(filepath):
  return static_file(filepath, root='public')

@app.route('/play', method='GET')
def get_play():
  # get some player info
  status = player.status()
  src = status['uri']
  if len(src) == 0 or status['playing']:
    src = get_next_stream_source()

  ret = player.play_stream(src)
  return {"playing": ret}

@app.route('/play-uri', method='PUT')
def put_play_uri():
  #get the uri from the params
  uri = request.json.get('uri')
  ret = player.play_stream(uri)
  return {"playing": ret}

@app.route('/streams', method='GET')
def get_streams():
  return Config["streams"]

@app.route('/volume', method='GET')
def get_volume():
  vol = player.get_volume()
  return {"volume": vol}

@app.route('/volume/<vol>', method='PUT')
def put_volume(vol):
  ret = player.volume(vol)
  return {"volume": ret}

@app.route('/stop', method='GET')
def get_stop():
  ret = player.stop()
  return {ret}

@app.route('/status', method='GET')
def get_status():
  #get the player status
  ret = {}
  ret['config'] = Config
  ret['player'] = player.status()
  return ret


# get the next stream from the config
def  get_next_stream_source():
  index = Config['current_stream_index']
  # is there already a current_stream?
  if index != None:
    index+=1
    if index==len(Config['streams']):
      index=0
  else:
    index= 0
  Config['current_stream_index'] = index
  stream = Config['streams'][index]
  return stream['audioSource']


#run the server app
app.run( host='0.0.0.0', port=8080, server='gevent')


