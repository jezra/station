//TODO: switch to fetch API

"use strict";
let currentStream=Object;
let playing, configStreams;
let streamsChanged = false;

//make it less verbose to find by id
function $(id){ return document.getElementById(id); }

//on page load, start checking the status
function init(){ checkStatus(); }

//the play button is clicked
function clickPlay(){
  var ajax = new XMLHttpRequest();
  ajax.open("GET", "/play", true);
  ajax.send();
}

//stop button is clicked
function clickStop(){
  var ajax = new XMLHttpRequest();
  ajax.open("GET", "/stop", true);
  ajax.send();
}

function clickStreamButton(event){

  fetch("/play-uri", {
    method: "PUT", // *GET, POST, PUT, DELETE, etc.
    body: JSON.stringify({"uri":event.target.targetUri}),
    headers: {
      "Content-Type": "application/json",
    },
  });
}

//volume range chas changed
function changedVolume(obj){
  var ajax = new XMLHttpRequest();
  ajax.open("PUT", "volume/"+obj.value, true);
  ajax.send();
}

// poll to check the status
function checkStatus(){
  //checkstatus in a bit
  setTimeout(checkStatus, 3000); //apparently a 'bit' is 3 seconds
  var ajax = new XMLHttpRequest();
  //what happens once data is loaded?
  ajax.onload = function(e) {
    let response = JSON.parse(ajax.response);
    //change  the volume range element to match that player's volume
    let vol = parseInt(response.player.volume);
    $("volume").value = vol;
    $("volumeDisplay").innerHTML = vol;
    //get the uri of the stream
    let uri = response.player.uri;
    //has the currentStream uri changed?
    if (uri != currentStream.audioSource) {
      //loop the streams and get the name of the stream
      for( var i=0; i< response.config.streams.length; i++) {
        let stream = response.config.streams[i];
        if (uri == stream['audioSource']) {
          currentStream = stream;
          console.log(currentStream.name)
          break;
        }
      }
    }

    if ( response.player.playing ) {
      //set the title
      updateTitle(currentStream.name);
      $("audioSourceName").innerHTML = currentStream.name;
    } else {
      //set the title
      updateTitle();
      $("audioSourceName").innerHTML = "&nbsp;";
    }

    /* has the stream list changed? */
    //get the streams
    let stringifiedStreams = JSON.stringify(response.config.streams)
    if (configStreams != stringifiedStreams) {
      configStreams = stringifiedStreams;
      /* create some UI */
      //clear the configStreams
      let cs = $("configStreams");
      while (cs.firstChild) {
        cs.removeChild(cs.firstChild);
      }
      // loop through the streams
      response.config.streams.forEach((stream)=>{
        //create buttons in list items
        let btn = document.createElement('button');
        btn.innerText = stream.name;
        btn.targetUri=stream.audioSource;
        btn.classList.add('streamButton');
        //connect a click action
        btn.addEventListener("click", clickStreamButton);
        //add the item to the configStreams
        $('configStreams').appendChild(btn);
      });

    }
  }
  ajax.open("GET", "status", true);
  ajax.send();
}

function updateTitle(str){
  document.title = (str) ? "Station: "+str : "Station";
}
