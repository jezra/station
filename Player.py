# Copright 2019 Jezra
# Released under the GPLv3
# see: http://www.gnu.org/licenses/gpl-3.0.html

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
import os

#initialize gstreamer
Gst.init(None)

## Player ##
class Player (GObject.GObject):
  __gsignals__ = {
    'finished-playing':(
      GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE, (int,)
    ),
  }
  current_stream = ""

  def __init__(self, volume=None):
    GObject.GObject.__init__(self)
    self.playbin = Gst.ElementFactory.make("playbin", "player")
    bus = self.playbin.get_bus()
    bus.add_signal_watch()
    bus.connect("message", self.on_message)
    self.playing = False
    #set the volume
    self.volume(volume)
    #buffer the player with a few K of data
    self.playbin.set_property('buffer-size', 56000)

  def stop(self):
    self.playing = False
    self.playbin.set_state(Gst.State.NULL)
    return "stop"
    
  def play_stream(self, stream_path):
    self.stop()
    self.playing = True
    self.playbin.set_property("uri", stream_path)
    self.playbin.set_state(Gst.State.PLAYING)
    self.current_stream = stream_path
    return stream_path

  def volume(self, value=None):
    # volume will be represented as a percentile
    if not value:
      #return playbin vol as percent
      vol = self.playbin.get_property("volume")
      return vol*100
    
    #use a float for fine control  
    value = float(value)
    #sanitize input
    if (value >=0 and value<=100 ):
      vol = (value*1.0)/100
    self.playbin.set_property("volume", vol)
    return vol 

  def status(self):
    status={}
    #what is good to know?

    # volume
    status['volume'] = self.volume()
    # playing?
    status['playing'] = self.playing
    # loaded source?
    status['uri'] = self.current_stream
    return status
    
  def on_message(self, bus, message):
    t = message.type
    if t == Gst.MessageType.EOS:
      self.playing = False
      self.playbin.set_state(Gst.State.NULL)
      self.emit('finished-playing',1)
      
    elif t == Gst.MessageType.ERROR:
      self.playing = False
      self.playbin.set_state(Gst.State.NULL)
      self.emit('finished-playing',1)
     
    elif t == Gst.MessageType.BUFFERING:
      print("buffering")
      #buffering, bummer. 
      self.stop()
      
    
